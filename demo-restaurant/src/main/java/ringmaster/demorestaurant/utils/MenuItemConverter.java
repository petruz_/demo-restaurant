package ringmaster.demorestaurant.utils;

import ringmaster.demorestaurant.api.dto.MenuItemDTO;
import ringmaster.demorestaurant.model.db.MenuItem;
import ringmaster.demorestaurant.model.vo.MenuItemVO;

import java.util.List;
import java.util.stream.Collectors;

public class MenuItemConverter {

//    @Override
//    public MenuItemDTO fromEntityToDto(MenuItem entity) {
//        return new MenuItemDTO(
//                entity.getCode(),
//                entity.getName(),
//                entity.getPrice(),
//                entity.isAvailable()
//        );
//    }
//
//    public MenuItem fromDtoToEntity(MenuItemDTO dto) {
//        return new MenuItem(
//                dto.getCode(),
//                dto.getName(),
//                dto.getPrice(),
//                dto.isAvailable()
//        );
//    }
//
//    public List<MenuItem> fromDtoToEntity(List<MenuItemDTO> dtos) {
//        return dtos.stream()
//                .map(dto -> this.fromDtoToEntity(dto))
//                .collect(Collectors.toList());
//    }

    ///////////////////////////////////////

    public static MenuItemVO dto2vo(MenuItemDTO dto) {
        MenuItemVO vo = new MenuItemVO();
        vo.setCode(dto.getCode());
        vo.setName(dto.getName());
        vo.setPrice(dto.getPrice());
        vo.setAvailable(dto.getAvailable());

        return vo;
    }

    public static List<MenuItemVO> dto2vo(List<MenuItemDTO> dtos) {
        return dtos.stream()
                .map(MenuItemConverter::dto2vo)
                .collect(Collectors.toList());
    }

    public static MenuItemDTO vo2dto(MenuItemVO vo) {
        MenuItemDTO dto = new MenuItemDTO();
        dto.setCode(vo.getCode());
        dto.setName(vo.getName());
        dto.setPrice(vo.getPrice());
        dto.setAvailable(vo.getAvailable());

        return dto;
    }

    public static List<MenuItemDTO> vo2dto(List<MenuItemVO> vos) {
        return vos.stream()
                .map(MenuItemConverter::vo2dto)
                .collect(Collectors.toList());
    }

    public static MenuItem vo2entity(MenuItemVO vo) {
        MenuItem entity = new MenuItem();
        entity.setCode(vo.getCode());
        entity.setName(vo.getName());
        entity.setPrice(vo.getPrice());
        entity.setAvailable(vo.getAvailable());

        return entity;
    }

    public static List<MenuItem> vo2entity(List<MenuItemVO> vos) {
        return vos.stream()
                .map(MenuItemConverter::vo2entity)
                .collect(Collectors.toList());
    }

    public static MenuItemVO entity2vo(MenuItem entity) {
        MenuItemVO vo = new MenuItemVO();
        vo.setCode(entity.getCode());
        vo.setName(entity.getName());
        vo.setPrice(entity.getPrice());
        vo.setAvailable(entity.isAvailable());

        return vo;
    }

    public static List<MenuItemVO> entity2vo(List<MenuItem> entities) {
        return entities.stream()
                .map(e -> entity2vo(e))
                .collect(Collectors.toList());
    }

}
