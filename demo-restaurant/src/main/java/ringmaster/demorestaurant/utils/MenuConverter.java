package ringmaster.demorestaurant.utils;

import ringmaster.demorestaurant.api.dto.MenuRequestDTO;
import ringmaster.demorestaurant.api.dto.MenuResponseDTO;
import ringmaster.demorestaurant.model.db.Menu;
import ringmaster.demorestaurant.model.db.Restaurant;
import ringmaster.demorestaurant.model.vo.MenuInVO;
import ringmaster.demorestaurant.model.vo.MenuOutVO;

public class MenuConverter {

    public static MenuInVO dto2vo(MenuRequestDTO dto) {
        MenuInVO vo = new MenuInVO();
        vo.setMenuItems( MenuItemConverter.dto2vo(dto.getMenuItems()) );

        return vo;
    }

    public static MenuResponseDTO vo2dto(MenuOutVO vo) {
        MenuResponseDTO dto = new MenuResponseDTO();
        dto.setId(vo.getId());
        dto.setRestaurantId(vo.getRestaurantId());
        dto.setMenuItems( MenuItemConverter.vo2dto(vo.getMenuItems()) );

        return dto;
    }

    public static Menu vo2entity(MenuInVO vo, Restaurant restaurant) {
        Menu entity = new Menu();
        entity.setMenuItems( MenuItemConverter.vo2entity(vo.getMenuItems()) );
        entity.setRestaurant(restaurant);

        return entity;
    }

    public static MenuOutVO entity2vo(Menu entity) {
        MenuOutVO vo = new MenuOutVO();
        vo.setId(entity.getId());
        vo.setRestaurantId(entity.getRestaurant().getId());
        vo.setMenuItems( MenuItemConverter.entity2vo(entity.getMenuItems()) );

        return vo;
    }
}
