package ringmaster.demorestaurant.utils;

import ringmaster.demorestaurant.api.dto.CreateRestaurantRequestDTO;
import ringmaster.demorestaurant.api.dto.RestaurantResponseDTO;
import ringmaster.demorestaurant.model.db.Restaurant;
import ringmaster.demorestaurant.model.vo.RestaurantInVO;
import ringmaster.demorestaurant.model.vo.RestaurantOutVO;

import java.util.List;
import java.util.stream.Collectors;

public class RestaurantConverter {

    public static RestaurantInVO dto2vo(CreateRestaurantRequestDTO dto) {
        RestaurantInVO vo = new RestaurantInVO();
        vo.setName(dto.getName());
        vo.setLocation(dto.getLocation());

        return vo;
    }

    public static RestaurantResponseDTO vo2dto(RestaurantOutVO vo) {
        RestaurantResponseDTO dto = new RestaurantResponseDTO();
        dto.setId(vo.getId());
        dto.setName(vo.getName());
        dto.setLocation(vo.getLocation());

        return dto;
    }

    public static Restaurant vo2entity(RestaurantInVO vo) {
        Restaurant entity = new Restaurant();
        entity.setName(vo.getName());
        entity.setLocation(vo.getLocation());
        entity.setMenu(null);

        return entity;
    }

    public static RestaurantOutVO entity2vo(Restaurant entity) {
        RestaurantOutVO vo = new RestaurantOutVO();
        vo.setId(entity.getId());
        vo.setName(entity.getName());
        vo.setLocation(entity.getLocation());

        return vo;
    }

    public static List<RestaurantOutVO> entity2voList(List<Restaurant> entities) {
        return entities.stream()
                .map(RestaurantConverter::entity2vo)
                .collect(Collectors.toList());
    }

    public static List<RestaurantResponseDTO> vo2dtoList(List<RestaurantOutVO> vos) {
        return vos.stream()
                .map(RestaurantConverter::vo2dto)
                .collect(Collectors.toList());
    }

}
