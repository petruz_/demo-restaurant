package ringmaster.demorestaurant.api.dto;

import java.util.List;

public class MenuRequestDTO {

    private List<MenuItemDTO> menuItems;

    public MenuRequestDTO() {
    }

    public List<MenuItemDTO> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemDTO> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public String toString() {
        return "MenuRequestDTO{" +
                "menuItems=" + menuItems +
                '}';
    }
}
