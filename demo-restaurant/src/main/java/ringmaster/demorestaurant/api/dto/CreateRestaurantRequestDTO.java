package ringmaster.demorestaurant.api.dto;

public class CreateRestaurantRequestDTO {

    private String name;
    private String location;

    public CreateRestaurantRequestDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "CreateRestaurantRequestDTO{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
