package ringmaster.demorestaurant.api.dto;

import java.util.List;

public class MenuResponseDTO {

    private Long id;
    private Long restaurantId;
    private List<MenuItemDTO> menuItems;

    public MenuResponseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public List<MenuItemDTO> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemDTO> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public String toString() {
        return "MenuResponseDTO{" +
                "id=" + id +
                ", restaurantId=" + restaurantId +
                ", menuItems=" + menuItems +
                '}';
    }
}
