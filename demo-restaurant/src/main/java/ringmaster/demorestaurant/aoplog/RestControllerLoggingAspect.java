package ringmaster.demorestaurant.aoplog;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;

@Component
@Aspect
@Order(0)
public class RestControllerLoggingAspect {

    private final String POST = "POST";
    private final String GET = "GET";
    private final String PUT = "PUT";
    private final String DELETE = "DELETE";

    private final static Logger logger = LoggerFactory.getLogger(RestControllerLoggingAspect.class);

    /* Pointcut for the Rest controllers */
    @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void postAction() {}

    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
    public void getAction() {}

    @Pointcut("@annotation(org.springframework.web.bind.annotation.PutMapping)")
    public void putAction() {}

    @Pointcut("@annotation(org.springframework.web.bind.annotation.DeleteMapping)")
    public void deleteAction() {}


    /* Executed before running POST method */
    @Before("postAction()")
    public void logBeforePostMethod(JoinPoint joinPoint) {
        logIncomingRequest(joinPoint, POST);
    }

    /* Executed before running GET method */
    @Before("getAction()")
    public void logBeforeGetMethod(JoinPoint joinPoint) {
        logIncomingRequest(joinPoint, GET);
    }

    /* Executed before running PUT method */
    @Before("putAction()")
    public void logBeforePutMethod(JoinPoint joinPoint) {
        logIncomingRequest(joinPoint, PUT);
    }

    /* Executed before running DELETE method */
    @Before("deleteAction()")
    public void logBeforeDeleteMethod(JoinPoint joinPoint) {
        logIncomingRequest(joinPoint, DELETE);
    }

    /* Log methods */
    private void logIncomingRequest(JoinPoint joinPoint, String action) {
        String url = getRequestUrl(joinPoint, action);
        String payload = getPayload(joinPoint);
        logger.info("{} {} - Payload = {{}}", action, url, payload);
    }

    private String getRequestUrl(JoinPoint joinPoint, String action) {
        Class<?> clazz = joinPoint.getTarget().getClass();
        RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        if (action.equals(POST)) {
            PostMapping methodPostMapping = method.getAnnotation(PostMapping.class);
            return getPostUrl(requestMapping, methodPostMapping);
        }
        if (action.equals(GET)) {
            GetMapping methodGetMapping = method.getAnnotation(GetMapping.class);
            return getGetUrl(requestMapping, methodGetMapping);
        }
        if (action.equals(PUT)) {
            PutMapping methodPutMapping = method.getAnnotation(PutMapping.class);
            return getPutUrl(requestMapping, methodPutMapping);
        }
        if (action.equals(DELETE)) {
            DeleteMapping methodDeleteMapping = method.getAnnotation(DeleteMapping.class);
            return getDeleteUrl(requestMapping, methodDeleteMapping);
        }

        return null;
    }

    private String getPostUrl(RequestMapping requestMapping, PostMapping postMapping) {
        String baseUrl = getBaseUrl(requestMapping);
        String endpoint = getUrl(postMapping.value());
        return baseUrl + endpoint;
    }

    private String getGetUrl(RequestMapping requestMapping, GetMapping getMapping) {
        String baseUrl = getBaseUrl(requestMapping);
        String endpoint = getUrl(getMapping.value());
        return baseUrl + endpoint;
    }

    private String getPutUrl(RequestMapping requestMapping, PutMapping putMapping) {
        String baseUrl = getBaseUrl(requestMapping);
        String endpoint = getUrl(putMapping.value());
        return baseUrl + endpoint;
    }

    private String getDeleteUrl(RequestMapping requestMapping, DeleteMapping deleteMapping) {
        String baseUrl = getBaseUrl(requestMapping);
        String endpoint = getUrl(deleteMapping.value());
        return baseUrl + endpoint;
    }

    private String getBaseUrl(RequestMapping requestMapping) {
        if (requestMapping == null)
            return "";
        return getUrl(requestMapping.value());
    }

    private String getUrl(String[] urls) {
        if (urls.length == 0)
            return "";
        return urls[0];
    }

    private String getPayload(JoinPoint joinPoint) {
        CodeSignature signature = (CodeSignature) joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        StringBuilder builder = new StringBuilder();

        for (int i=0; i < args.length; i++) {
            if (i != 0)
                builder.append(", ");
            String parameterName = signature.getParameterNames()[i];
            builder.append(parameterName);
            builder.append(": ");
            builder.append(args[i].toString());
        }
        return builder.toString();
    }
}
