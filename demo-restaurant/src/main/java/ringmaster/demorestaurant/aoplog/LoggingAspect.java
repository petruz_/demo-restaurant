package ringmaster.demorestaurant.aoplog;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
@Order(1)
public class LoggingAspect {

    private final static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    /* Pointcut for this application Spring Beans */
    @Pointcut("execution(public * ringmaster.demorestaurant..*.*(..))")
    public void springBeanMethods() {}

    @Pointcut("execution(public void ringmaster.demorestaurant..*.*(..))")
    public void springBeanVoidMethods() {}

    /* Log methods */
    private void logInvocation(JoinPoint joinPoint) {
//        final String className = joinPoint.getSignature().getDeclaringTypeName();
        final String className = joinPoint.getSignature().getDeclaringType().getSimpleName();
        final String methodName = joinPoint.getSignature().getName();
        final String args = Arrays.toString(joinPoint.getArgs());
        logger.info("Enter: {}.{} with args = {}", className, methodName, args);
    }

    private void logTermination(JoinPoint joinPoint, Object retValue) {
        final String className = joinPoint.getSignature().getDeclaringType().getSimpleName();
        final String methodName = joinPoint.getSignature().getName();
        logger.info("Exit: {}.{} -> {}", className, methodName, retValue);
    }

    private void logVoidTermination(JoinPoint joinPoint) {
        final String className = joinPoint.getSignature().getDeclaringType().getSimpleName();
        final String methodName = joinPoint.getSignature().getName();
        logger.info("Exit: {}.{} -> RETURN", className, methodName);
    }

    private void logException(JoinPoint joinPoint, Throwable exception) {
        final String className = joinPoint.getSignature().getDeclaringType().getSimpleName();
        final String methodName = joinPoint.getSignature().getName();
//        final String args = Arrays.toString(joinPoint.getArgs());
        logger.error("Exception in {}.{} with cause = {}", className, methodName, exception.toString());
    }

    /* Executed before running the method */
    @Before("springBeanMethods()")
    public void logBeforeExecuteMethod(JoinPoint joinPoint) {
        logInvocation(joinPoint);
    }

    /* Executed when the method has finished (successfully) */
    @AfterReturning(value = "springBeanMethods() && !springBeanVoidMethods()", returning = "retValue")
    public void logSuccessMethod(JoinPoint joinPoint, Object retValue) {
        logTermination(joinPoint, retValue);
    }

    /* Executed when the void method has finished (successfully) */
    @AfterReturning("springBeanVoidMethods()")
    public void logSuccessVoidMethod(JoinPoint joinPoint) {
        logVoidTermination(joinPoint);
    }

    /* Executed if an exception was thrown */
    @AfterThrowing(value = "springBeanMethods()", throwing = "exception")
    public void logErrorApplication(JoinPoint joinPoint, Throwable exception) {
        logException(joinPoint, exception);
    }
}
