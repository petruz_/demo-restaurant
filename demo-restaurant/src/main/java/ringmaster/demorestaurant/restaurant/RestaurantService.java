package ringmaster.demorestaurant.restaurant;

import ringmaster.demorestaurant.model.vo.MenuInVO;
import ringmaster.demorestaurant.model.vo.MenuOutVO;
import ringmaster.demorestaurant.model.vo.RestaurantInVO;
import ringmaster.demorestaurant.model.vo.RestaurantOutVO;

import java.util.List;

public interface RestaurantService {

    /**
     * Creates and returns a new restaurant.
     *
     * @param request {@code RestaurantInVo} with the input data
     * @return the new restaurant
     */
    RestaurantOutVO createRestaurant(RestaurantInVO request);

    List<RestaurantOutVO> getAllRestaurants();

    /**
     * Returns the restaurant with the corresponding id. If the restaurant doesn't exist returns null.
     *
     * @param id the restaurant id
     * @return a {@code RestaurantOutVo} or null
     */
    RestaurantOutVO getRestaurantById(Long id);

    MenuOutVO createOrUpdateMenu(Long restaurantId, MenuInVO menuRequest);

    MenuOutVO getMenuByRestaurantId(Long restaurantId);

}
