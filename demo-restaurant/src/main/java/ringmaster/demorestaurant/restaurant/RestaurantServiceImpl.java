package ringmaster.demorestaurant.restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ringmaster.demorestaurant.exception.ErrorCode;
import ringmaster.demorestaurant.model.db.Menu;
import ringmaster.demorestaurant.model.db.Restaurant;
import ringmaster.demorestaurant.model.vo.MenuInVO;
import ringmaster.demorestaurant.model.vo.MenuOutVO;
import ringmaster.demorestaurant.model.vo.RestaurantInVO;
import ringmaster.demorestaurant.model.vo.RestaurantOutVO;
import ringmaster.demorestaurant.utils.MenuConverter;
import ringmaster.demorestaurant.utils.RestaurantConverter;

import java.util.List;

@Service
@Transactional
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private MenuRepository menuRepository;

    public RestaurantOutVO createRestaurant(RestaurantInVO request) {
        Restaurant restaurant = RestaurantConverter.vo2entity(request);
        restaurant = this.restaurantRepository.save(restaurant);

        return RestaurantConverter.entity2vo(restaurant);
    }

    public List<RestaurantOutVO> getAllRestaurants() {
        List<Restaurant> allRestaurants = this.restaurantRepository.findAll();
        return RestaurantConverter.entity2voList(allRestaurants);
    }

    public RestaurantOutVO getRestaurantById(Long id) {
        Restaurant restaurant = this.restaurantRepository.findById(id).orElse(null);
        if (restaurant == null)
            return new RestaurantOutVO(ErrorCode.NOT_FOUND.getCode(), "No restaurant found with ID " + id);

        return RestaurantConverter.entity2vo(restaurant);
    }

    public MenuOutVO createOrUpdateMenu(Long restaurantId, MenuInVO menuRequest) {
        Restaurant restaurant = this.restaurantRepository.findById(restaurantId).orElse(null);
        if (restaurant == null)
            return new MenuOutVO(ErrorCode.NOT_FOUND.getCode(), "No restaurant found with ID " + restaurantId);

        Menu menu = MenuConverter.vo2entity(menuRequest, restaurant);
        if (restaurant.getMenu() != null)
            this.menuRepository.delete(restaurant.getMenu());
        Menu newMenu = this.menuRepository.save(menu);

        return MenuConverter.entity2vo(newMenu);
    }

    public MenuOutVO getMenuByRestaurantId(Long restaurantId) {
        Restaurant restaurant = this.restaurantRepository.findById(restaurantId).orElse(null);
        if (restaurant == null)
            return new MenuOutVO(ErrorCode.NOT_FOUND.getCode(), "No restaurant found with ID " + restaurantId);

        Menu menu = restaurant.getMenu();
        if (menu == null)
            return new MenuOutVO(ErrorCode.NOT_FOUND.getCode(), "No menu found for the restaurant with ID " + restaurantId);

        return MenuConverter.entity2vo(menu);
    }

}
