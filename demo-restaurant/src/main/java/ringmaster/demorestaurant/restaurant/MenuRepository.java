package ringmaster.demorestaurant.restaurant;

import org.springframework.data.jpa.repository.JpaRepository;
import ringmaster.demorestaurant.model.db.Menu;

public interface MenuRepository extends JpaRepository<Menu, Long> {

//    Menu findByRestaurantId(Long restaurantId);

}
