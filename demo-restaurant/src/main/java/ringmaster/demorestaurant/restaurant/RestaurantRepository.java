package ringmaster.demorestaurant.restaurant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ringmaster.demorestaurant.model.db.Restaurant;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {

//    @Query("SELECT r " +
//            "FROM Restaurant r LEFT JOIN FETCH r.menu.menuItems " +
//            "WHERE r.id = :id"
//    )
//    Restaurant findByIdWithMenu(@Param("id") Long id);

}
