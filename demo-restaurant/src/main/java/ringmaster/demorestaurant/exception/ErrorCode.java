package ringmaster.demorestaurant.exception;

public enum ErrorCode {
    REQUEST_BODY_MISSING("error-001", "Request body missing"),
    REQUEST_BODY_EMPTY("error-002", "Request body empty"),
    MANDATORY_FIELD("error-003", "Mandatory field"),
    NOT_FOUND("error-004", "Not found"),
    ID_NULL("error-005", "ID null");

    private final String code;
    private final String reasonPhrase;

    ErrorCode(String code, String reasonPhrase) {
        this.code = code;
        this.reasonPhrase = reasonPhrase;
    }

    public String getCode() {
        return code;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }
}
