package ringmaster.demorestaurant.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ringmaster.demorestaurant.api.dto.ErrorDTO;

@RestControllerAdvice
public class RestaurantExceptionHandler {

    @ExceptionHandler(DemorestaurantException.class)
    public ResponseEntity<ErrorDTO> toResponse(DemorestaurantException ex) {
        HttpStatus status;

        if (ErrorCode.REQUEST_BODY_MISSING.getCode().equals(ex.getCode())
            || ErrorCode.REQUEST_BODY_EMPTY.getCode().equals(ex.getCode())
            || ErrorCode.MANDATORY_FIELD.getCode().equals(ex.getCode())
        ) {
            status = HttpStatus.BAD_REQUEST;
        } else if (ErrorCode.NOT_FOUND.getCode().equals(ex.getCode())) {
            status = HttpStatus.NOT_FOUND;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return buildErrorResponse(ex, status);
    }

    private ResponseEntity<ErrorDTO> buildErrorResponse(DemorestaurantException ex, HttpStatus httpStatus) {
        ErrorDTO errorDto = new ErrorDTO(ex.getCode(), ex.getDescription());
        return ResponseEntity
                .status(httpStatus)
                .body(errorDto);
    }
}
