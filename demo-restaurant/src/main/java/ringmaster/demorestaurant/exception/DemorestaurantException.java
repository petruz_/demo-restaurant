package ringmaster.demorestaurant.exception;

public class DemorestaurantException extends RuntimeException {

    private String code;
    private String description;

    public DemorestaurantException() {
        super();
    }

    public DemorestaurantException(ErrorCode code, String description) {
        this(code.getCode(), description);
    }

    public DemorestaurantException(String code, String description) {
        super("Error code: " + code + ",\tdescription: " + description);
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "DemorestaurantException{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
