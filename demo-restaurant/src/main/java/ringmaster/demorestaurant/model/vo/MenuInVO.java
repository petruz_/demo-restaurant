package ringmaster.demorestaurant.model.vo;

import java.util.List;

public class MenuInVO {

    private List<MenuItemVO> menuItems;

    public MenuInVO() {
    }

    public List<MenuItemVO> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemVO> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuInVO menuInVO = (MenuInVO) o;

        return menuItems != null ? menuItems.equals(menuInVO.menuItems) : menuInVO.menuItems == null;
    }

    @Override
    public int hashCode() {
        return menuItems != null ? menuItems.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "MenuInVO{" +
                "menuItems=" + menuItems +
                '}';
    }
}
