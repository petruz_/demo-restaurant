package ringmaster.demorestaurant.model.vo;

import java.util.List;

public class MenuOutVO extends BaseOutVO {

    private Long id;
    private Long restaurantId;
    private List<MenuItemVO> menuItems;

    public MenuOutVO() {
    }

    public MenuOutVO(String code, String description) {
        super(code, description);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public List<MenuItemVO> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemVO> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuOutVO menuOutVO = (MenuOutVO) o;

        if (id != null ? !id.equals(menuOutVO.id) : menuOutVO.id != null) return false;
        if (restaurantId != null ? !restaurantId.equals(menuOutVO.restaurantId) : menuOutVO.restaurantId != null)
            return false;
        return menuItems != null ? menuItems.equals(menuOutVO.menuItems) : menuOutVO.menuItems == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (restaurantId != null ? restaurantId.hashCode() : 0);
        result = 31 * result + (menuItems != null ? menuItems.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MenuOutVO{" +
                "id=" + id +
                ", restaurantId=" + restaurantId +
                ", menuItems=" + menuItems +
                '}';
    }
}
