package ringmaster.demorestaurant.model.db;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Menu {

    @Id
    @GeneratedValue
    private Long id;

    @ElementCollection
    private List<MenuItem> menuItems;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    public Menu() {
        this.menuItems = new ArrayList<>();
    }

    public Menu(Restaurant restaurant) {
        this();
        this.restaurant = restaurant;
    }

    public Menu(List<MenuItem> menuItems, Restaurant restaurant) {
        this.menuItems = menuItems;
        this.restaurant = restaurant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu = (Menu) o;

        if (id != null ? !id.equals(menu.id) : menu.id != null) return false;
        if (menuItems != null ? !menuItems.equals(menu.menuItems) : menu.menuItems != null) return false;
        return restaurant.equals(menu.restaurant);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (menuItems != null ? menuItems.hashCode() : 0);
        result = 31 * result + restaurant.getId().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", menuItems=" + menuItems +
                ", restaurantId=" + restaurant.getId() +
                '}';
    }
}
