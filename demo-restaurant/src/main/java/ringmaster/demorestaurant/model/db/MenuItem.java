package ringmaster.demorestaurant.model.db;

import javax.persistence.Embeddable;

@Embeddable
public class MenuItem {

    private String code;
    private String name;
    private Double price;
    private Boolean available;

    public MenuItem() {
    }

    public MenuItem(String code, String name, Double price) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.available = true;
    }

    public MenuItem(String code, String name, Double price, Boolean available) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.available = available;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuItem menuItem = (MenuItem) o;

        if (!code.equals(menuItem.code)) return false;
        if (!name.equals(menuItem.name)) return false;
        if (price != null ? !price.equals(menuItem.price) : menuItem.price != null) return false;
        return available != null ? available.equals(menuItem.available) : menuItem.available == null;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (available != null ? available.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", available=" + available +
                '}';
    }
}
