package ringmaster.demorestaurant.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ringmaster.demorestaurant.api.dto.CreateRestaurantRequestDTO;
import ringmaster.demorestaurant.api.dto.MenuRequestDTO;
import ringmaster.demorestaurant.api.dto.MenuResponseDTO;
import ringmaster.demorestaurant.api.dto.RestaurantResponseDTO;
import ringmaster.demorestaurant.exception.DemorestaurantException;
import ringmaster.demorestaurant.exception.ErrorCode;
import ringmaster.demorestaurant.model.vo.MenuInVO;
import ringmaster.demorestaurant.model.vo.MenuOutVO;
import ringmaster.demorestaurant.model.vo.RestaurantInVO;
import ringmaster.demorestaurant.model.vo.RestaurantOutVO;
import ringmaster.demorestaurant.restaurant.RestaurantService;
import ringmaster.demorestaurant.utils.MenuConverter;
import ringmaster.demorestaurant.utils.RestaurantConverter;

import java.util.List;

@RestController
public class RestaurantRestController {

    @Autowired
    private RestaurantService restaurantService;

    @PostMapping("/restaurants")
    @ResponseStatus(HttpStatus.CREATED)
    public Long createRestaurant(@RequestBody CreateRestaurantRequestDTO request) {
        checkRestaurantRequestBody(request);

        RestaurantInVO restaurant = RestaurantConverter.dto2vo(request);
        RestaurantOutVO response = this.restaurantService.createRestaurant(restaurant);

        return response.getId();
    }

    private void checkRestaurantRequestBody(CreateRestaurantRequestDTO restaurantDto) {
        if (restaurantDto == null) {
            throw new DemorestaurantException(ErrorCode.REQUEST_BODY_MISSING, "Request body is missing.");
        }
        if (restaurantDto.getName() == null || restaurantDto.getName().isEmpty()) {
            throw new DemorestaurantException(ErrorCode.MANDATORY_FIELD, "Mandatory 'name' field is null or empty.");
        }
        if (restaurantDto.getLocation() == null || restaurantDto.getLocation().isEmpty()) {
            throw new DemorestaurantException(ErrorCode.MANDATORY_FIELD, "Mandatory 'location' field is null or empty.");
        }
    }

    @GetMapping("/restaurants")
    public List<RestaurantResponseDTO> getRestaurants() {
        List<RestaurantOutVO> restaurants = this.restaurantService.getAllRestaurants();
        return RestaurantConverter.vo2dtoList(restaurants);
    }

    @GetMapping("/restaurants/{restaurantId}")
    public RestaurantResponseDTO getRestaurant(@PathVariable Long restaurantId) throws DemorestaurantException {
        if (restaurantId == null)
            throw new DemorestaurantException(ErrorCode.ID_NULL, "ID param cannot be null.");

        RestaurantOutVO restaurant = this.restaurantService.getRestaurantById(restaurantId);
        if (restaurant.getCode() != null)
            throw new DemorestaurantException(restaurant.getCode(), restaurant.getDescription());

        return RestaurantConverter.vo2dto(restaurant);
    }

    @PutMapping("/restaurants/{restaurantId}/menu")
    @ResponseStatus(HttpStatus.OK)
    public Long createOrUpdateMenu(@PathVariable Long restaurantId, @RequestBody MenuRequestDTO request) throws DemorestaurantException {
        if (restaurantId == null)
            throw new DemorestaurantException(ErrorCode.ID_NULL, "ID param cannot be null.");
        checkMenuRequestBody(request);

        MenuInVO menuRequest = MenuConverter.dto2vo(request);
        MenuOutVO newUpdatedMenu = this.restaurantService.createOrUpdateMenu(restaurantId, menuRequest);
        if (newUpdatedMenu.getCode() != null)
            throw new DemorestaurantException(newUpdatedMenu.getCode(), newUpdatedMenu.getDescription());

        MenuResponseDTO menuResponse = MenuConverter.vo2dto(newUpdatedMenu);
        return menuResponse.getId();
    }

    private void checkMenuRequestBody(MenuRequestDTO menuDto) throws DemorestaurantException {
        if (menuDto == null) {
            throw new DemorestaurantException(ErrorCode.REQUEST_BODY_MISSING, "Request body is missing.");
        }
        if (menuDto.getMenuItems() == null || menuDto.getMenuItems().isEmpty()) {
            throw new DemorestaurantException(ErrorCode.MANDATORY_FIELD, "Mandatory 'menuItems' field is null or empty.");
        }
    }

    @GetMapping("/restaurants/{restaurantId}/menu")
    public MenuResponseDTO getRestaurantMenu(@PathVariable Long restaurantId) throws DemorestaurantException {
        if (restaurantId == null)
            throw new DemorestaurantException(ErrorCode.ID_NULL, "ID param cannot be null.");

        MenuOutVO menu = this.restaurantService.getMenuByRestaurantId(restaurantId);
        if (menu.getCode() != null)
            throw new DemorestaurantException(menu.getCode(), menu.getDescription());

        return MenuConverter.vo2dto(menu);
    }

}
